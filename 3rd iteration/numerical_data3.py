import csv
import string

temp_userid = 0

with open('events3.csv') as events_data:
    events_dict = csv.DictReader(events_data)
    events = list(events_dict)

    # all possible events that could occur, with their numerical representation
    event_int = []
    eventCat_int = []

    # dictionary mapping each numerical representation
    all_events = {}
    # dictionary mapping each numerical representation for event categories
    all_eventCat = {}

    for e in events:
        if event_int.count(e['Event Action']) == 0:
            event_int.append(e['Event Action'])

        if eventCat_int.count(e['Event Category']) == 0:
            eventCat_int.append(e['Event Category'])
    print('************************** Events ***********************')
    for index, action in enumerate(event_int):
        number = float(index + 1 * 1.50)
        all_events[action] = number
        print(str(number) + ' now represents the ' + action + ' action')
    print('************************** Event Categories ***********************')
    for index, action in enumerate(eventCat_int):
        number = float(index + 1 * 1.50)
        all_eventCat[action] = number
        print(str(number) + ' now represents the ' + action + ' action')

with open('pages3.csv') as page_data:
    page_dict = csv.DictReader(page_data)
    pages = list(page_dict)

    # all possible pages in the dataset, with their numerical representation
    pages_int = []
    # dictionary mapping each numerical representation
    all_pages = {}

    for page in pages:
        if pages_int.count(page['Page']) == 0:
            pages_int.append(page['Page'])
    print('************************** Pages ***********************')
    for index, page in enumerate(pages_int):
        page_no = float(index + 1.50) 
        all_pages[page] = page_no
      
        print(str(page_no) + ' now represents ' + page)

with open('products3.csv') as product_data:
    product_dict = csv.DictReader(product_data)
    productsCat = list(product_dict)

    # all possible product categories in the datatset with their numerical representation:
    productCat_int = []

    # dictionary mapping all each  numerical representation
    all_productCat = {}

    for cat in productsCat:
        if cat['Product Categories in'] != 'N/A':
            if productCat_int.count(cat['Product Categories in']) == 0:
                productCat_int.append(cat['Product Categories in'])

        if cat['Product Categories Out'] != 'N/A':
            if productCat_int.count(cat['Product Categories Out']) == 0:
                productCat_int.append(cat['Product Categories Out'])
    print('************************** Product Categories ***********************')
    for index, cat in enumerate(productCat_int):
        cat_no = float(index + 1.50) 
        all_productCat[cat] = cat_no
        print(str(cat_no) + ' now represents ' + cat)

with open('data3.csv') as data:
    data_dict = csv.DictReader(data)
    initial_data = list(data_dict)
    fields = initial_data[0].keys()
    
    # Here, I am replacing all values to numerical
    for data in initial_data:
        # removes the 'GA1.4.' before every id
        temp_userid += 1
        print(temp_userid + ' now represents ' + data['user_id'] )
        data['user_id'] = temp_userid

        newClientId = temp_userid * 1.05
        print(newClientId )
        data['client_id'] = temp_userid * 1.05


        # here we are looping through each column available in the data.csv
        for column in data:
            # if a column is in the list of all possible events, replace the value of what is
            # in the data.csv with its numerical representation.

            if data.get(column) in all_events: 
                for event in all_events:
                    if data.get(column) == event:
                        data[column] = all_events.get(event)
            elif data.get(column) in all_eventCat:
                for eventCat in all_eventCat:
                  if data.get(column) == eventCat:
                      data[column] = all_events.get(eventCat)

            elif data.get(column) in all_pages:
                for page in all_pages:
                    if data.get(column) == page:
                        data[column] = all_pages.get(page)
            elif data.get(column) in all_productCat:
                for cat in all_productCat:
                    if data.get(column) == cat:
                        data[column] = all_productCat.get(cat)

try:
    with open('numerical_data3.csv', 'w') as num_data:
        writer = csv.DictWriter(num_data, fieldnames=fields)
        writer.writeheader()
        for d in initial_data:
            writer.writerow(d)
        print('successfully created the numerical dataset.')
except IOError:
    print("I/O error") 

