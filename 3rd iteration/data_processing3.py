import csv
processed = []
columns = ['user_id', 'client_id','Hits']
i = 0

# Completed the main dimentions of the GA data
# getting details per unique user
with open('users3.csv') as users_data:
    user_dict = csv.DictReader(users_data)
    users = list(user_dict)
    
    for user in users:
        processed.append({
            'user_id': user['User ID'],
            'client_id':user['client ID'],
            'Hits': user['Hits'],
        })
    

with open('events3.csv') as events_data:
    events_dict = csv.DictReader(events_data)
    events = list(events_dict)

    for p in processed:
        for event in events:
            i += 1
            if p['user_id'] == event['User ID']:
                column = 'event-' + str(i)
                column2 = 'event-cat' + str(i)
                column3 = 'sessions'
                
                if column in columns:
                    p[column] = event['Event Action']
                elif column2 in columns:
                    p[column2] = event['Event Category']
                elif column3 in columns:
                    p[column3] = event['Sessions']
                else:
                    columns.append(column)
                    columns.append(column2)
                    columns.append(column3)
             
                    p[column] = event['Event Action']
                    p[column2] = event['Event Category']
                    p[column3] = event['Sessions']
            else:
                i = 0

with open('products3.csv') as products_data:
    products_dict = csv.DictReader(products_data)
    products = list(products_dict)

    for p in processed:
        for product in products:
            i += 1
            if p['user_id'] == product['User ID']:
                column = 'product-' + str(i)
                column2 = 'hovered in category ' + str(i)
                column3 = 'hovered out category' + str(i)
                if column in columns:
                    p[column] = product['Product ID']
                elif column2 in columns:
                    p[column2] = product['Product Categories in']
                elif column3 in columns:
                    p[column3] = product['Product Categories Out']
                else:
                    columns.append(column)
                    columns.append(column2)
                    columns.append(column3)
                    p[column] = product['Product ID']
                    p[column2] = product['Product Categories in']
                    p[column3] = product['Product Categories Out']
            else:
                i = 0


# with open('removed_items3.csv') as removed_products_data:
#     removed_products_dict = csv.DictReader(removed_products_data)
#     removed_products = list(removed_products_dict)

#     for p in processed:     
#         for r_product in removed_products:
#             i += 1
#             if p['user_id'] == product['User ID']:
#                 column = 'removed-product-' + str(i)
#                 if column in columns:
#                     p[column] = product['Product ID']
#                 else:
#                     columns.append(column)
#                     p[column] = product['Product ID']
#             else:
#                 i = 0


with open('pages3.csv') as page_visited:
    pages_dict = csv.DictReader(page_visited)
    pages = list(pages_dict)

    for p in processed:
        for page in pages:
            i +=1
            if p['user_id'] == page['User ID']:
                 column = 'page-' + str(i)
                 if column in columns:
                    p[column] = page['Page']
                 else:
                    columns.append(column)
                    p[column] = page['Page']
            else:
                i = 0

# # print(processed[0])
# # print('***********************************')
# # print(columns)

try:
    with open('data3.csv', 'w') as data:
        writer = csv.DictWriter(data, fieldnames=columns)
        writer.writeheader()
        for p in processed:
            writer.writerow(p)
except IOError:
    print("I/O error") 