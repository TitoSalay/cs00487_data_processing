import csv
processed = []
columns = ['id','Adding a product on to the cart (Goal 4 Completions)','Create an account (Goal 2 Completions)', 
'User Sign in (Goal 3 Completions)', 'Hits']
i = 0

# Completed the main dimentions of the GA data
# getting details per unique user
with open('users2.csv') as users_data:
    user_dict = csv.DictReader(users_data)
    users = list(user_dict)
    
    for user in users:
        processed.append({
            'id': user['ID'],
            'Hits': user['Hits'],
            'Adding a product on to the cart (Goal 4 Completions)': user['Adding a product on to the cart (Goal 4 Completions)'],
            'Create an account (Goal 2 Completions)': user['Create an account (Goal 2 Completions)'],
            'User Sign in (Goal 3 Completions)': user['User Sign in (Goal 3 Completions)']     
        })

with open('events2.csv') as events_data:
    events_dict = csv.DictReader(events_data)
    events = list(events_dict)

    for p in processed:
        for event in events:
            i += 1
            if p['id'] == event['ID']:
                column = 'event-' + str(i)
                if column in columns:
                    p[column] = event['Event Action']
                else:
                    columns.append(column)
                    p[column] = event['Event Action']
            else:
                i = 0

with open('products2.csv') as products_data:
    products_dict = csv.DictReader(products_data)
    products = list(products_dict)

    for p in processed:
        for product in products:
            i += 1
            if p['id'] == product['ID']:
                column = 'product-' + str(i)
                if column in columns:
                    p[column] = product['Product ID']
                else:
                    columns.append(column)
                    p[column] = product['Product ID']
            else:
                i = 0

with open('removed_items.csv') as removed_products_data:
    removed_products_dict = csv.DictReader(removed_products_data)
    removed_products = list(removed_products_dict)

    for p in processed:     
        for r_product in removed_products:
            i += 1
            if p['id'] == product['ID']:
                column = 'removed-product-' + str(i)
                if column in columns:
                    p[column] = product['Product ID']
                else:
                    columns.append(column)
                    p[column] = product['Product ID']
            else:
                i = 0


with open('pages2.csv') as page_visited:
    pages_dict = csv.DictReader(page_visited)
    pages = list(pages_dict)

    for p in processed:
        for page in pages:
            i +=1
            if p['id'] == page['ID']:
                 column = 'page-' + str(i)
                 if column in columns:
                    p[column] = page['Page']
                 else:
                    columns.append(column)
                    p[column] = page['Page']
            else:
                i = 0

# print(processed[0])
# print('***********************************')
# print(columns)

try:
    with open('data2.csv', 'w') as data:
        writer = csv.DictWriter(data, fieldnames=columns)
        writer.writeheader()
        for p in processed:
            writer.writerow(p)
except IOError:
    print("I/O error") 