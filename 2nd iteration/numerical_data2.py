import csv

with open('events2.csv') as events_data:
    events_dict = csv.DictReader(events_data)
    events = list(events_dict)

    # all possible events that could occur, with their numerical representation
    event_int = []

    # dictionary mapping each numerical representation
    all_events = {}

    for e in events:
        if event_int.count(e['Event Action']) == 0:
            event_int.append(e['Event Action'])
    
    for index, action in enumerate(event_int):
        number = float(index + 1 * 1.75)
        all_events[action] = number
        print(str(number) + ' now represents the ' + action + ' action')

with open('pages2.csv') as page_data:
    page_dict = csv.DictReader(page_data)
    pages = list(page_dict)

    # all possible pages in the dataset, with their numerical representation
    pages_int = []
    # dictionary mapping each numerical representation
    all_pages = {}

    for page in pages:
        if pages_int.count(page['Page']) == 0:
            pages_int.append(page['Page'])

    for index, page in enumerate(pages_int):
        page_no = float(index + 1.5) 
        all_pages[page] = page_no
        print(str(page_no) + ' now represents ' + page)

with open('data2.csv') as data:
    data_dict = csv.DictReader(data)
    initial_data = list(data_dict)
    fields = initial_data[0].keys()
    
    # Here, I am replacing all values to numerical
    for data in initial_data:
        data['id'] = data.get('id').strip('GA1.4.') # removes the 'GA1.4.' before every id
        print(data.get('id'))
        for column in data:
            if data.get(column) in all_events:
                for event in all_events:
                    if data.get(column) == event:
                        data[column] = all_events.get(event)
            elif data.get(column) in all_pages:
                for page in all_pages:
                    if data.get(column) == page:
                        data[column] = all_pages.get(page)


try:
    with open('numerical_data2.csv', 'w') as num_data:
        writer = csv.DictWriter(num_data, fieldnames=fields)
        writer.writeheader()
        for d in initial_data:
            writer.writerow(d)
        print('successfully created the numerical dataset.')
except IOError:
    print("I/O error") 

