************************** Events ***********************
1.5 now represents the Hovering out of a product action
2.5 now represents the Hovering over a product action
3.5 now represents the Hover over the new ins section action
4.5 now represents the Hovering in to the shop by category section action
5.5 now represents the Adding products to the cart action
6.5 now represents the Clicking on the add to cart from the product page action
7.5 now represents the Add to Cart Clicks action
8.5 now represents the Adding a product to the cart from the shop action
9.5 now represents the Adding products from the homepage action
10.5 now represents the Adding products to the cart from a product category action
11.5 now represents the removing product from cart from the cart page action
12.5 now represents the Removing product from the cart action
13.5 now represents the User purchasing items action
14.5 now represents the Clicking on related products action
15.5 now represents the Rating a product action
16.5 now represents the Viewing the details of a product from the homepage action
17.5 now represents the Proceeding to checkout from the cart action
18.5 now represents the Clicking on the next page of a product list action
19.5 now represents the Hovering of a zoomed item action
20.5 now represents the Using the search bar action
21.5 now represents the Hovering over the fan favourites section action
22.5 now represents the Clicks on a product image to view it in more details action
23.5 now represents the go to the shop from the home screen action
24.5 now represents the Clicking on a product from the shop page action
************************** Event Categories ***********************
1.5 now represents the User Interaction action
2.5 now represents the Cart action
3.5 now represents the Purchase action
4.5 now represents the Product action
5.5 now represents the User interaction action
6.5 now represents the User Interactions action
************************** Pages ***********************
1.5 now represents /product-category/accessories/
2.5 now represents /
3.5 now represents /product-category/all-costumes/
4.5 now represents /product-category/clothing/page/2/
5.5 now represents /shop/
6.5 now represents /product-category/clothing/
7.5 now represents /product-category/all-womens-shoes/
8.5 now represents /product-category/all-juniors-clothing/
9.5 now represents /product-category/all-mens-shoes/
10.5 now represents /product-category/athletic-apparel/
11.5 now represents /product-category/all-mens-clothing/
12.5 now represents /shop/page/4/
13.5 now represents /?s=kid
14.5 now represents /product-category/casual/
15.5 now represents /product/rubies-costume-adult-mens-regency-plush-santa-claus-christmas-suit-x-large/
16.5 now represents /product-category/all-womens-clothing/
17.5 now represents /product/sneed-womens-dance-shoes-latin-pu-low-heel-more-colors-leopard-us10-5-eu27-uk9-5-little-kids-leopard-us10-5-eu27-uk9-5-little-kids/
18.5 now represents /page/2/?s=nike&post_type=product
19.5 now represents /product/adrienne-vittadini-footwear-womens-sereen-dress-sandal/
20.5 now represents /product-category/boots/
21.5 now represents /?s=nike&post_type=product
22.5 now represents /product/new-era-cap-mens-logo-swipe-oakland-athletics-star-wars-9fifty-snapback-cap/
23.5 now represents /product-category/apparel-merchandise/
24.5 now represents /product-category/boxing/
25.5 now represents /product/native-miller-men-us-10-gray-loafer-uk-9-eu-43/?unapproved=6&moderation-hash=50192c8c774a4d5b834580ec374551fb
26.5 now represents /product-category/athletic-running-shoes/
27.5 now represents /product/comical-cow-halloween-costume-adult-size-one-size-fits-most/
28.5 now represents /product-category/baseball-caps/
29.5 now represents /product/allegra-k-womens-open-toe-chunky-mid-heel-ankle-strap-sandals-burgundy-size-6/
30.5 now represents /product/nike-free-5-0-preschool-kids-shoes-size/
31.5 now represents /product/nike-mens-cortez-ultra-qs-black-metallic-gold-black-882493-001/
32.5 now represents /product/puma-x-rise-blaze-of-glory-round-toe-leather-sneakers/
33.5 now represents /product-category/athletic/
34.5 now represents /product-category/all-womens-shoes/page/4/
35.5 now represents /product/nike-womens-victory-compression-sports-bra/
36.5 now represents /product/twisted-x-western-boots-mens-buckaroo-spur-ridge-crazy-horse-mbkl012/
37.5 now represents /product/adore-1008sq-7-heel-2-3-4-platform-ankle-boot/
38.5 now represents /?customize_changeset_uuid=354096e2-2744-479b-a13a-f3393bee8010&customize_theme=cs00487_fyp&customize_messenger_channel=preview-2&customize_autosaved=on
39.5 now represents /product/alfani-jeules-women-pointed-toe-synthetic-white-heels/
40.5 now represents /product/nike-lunar-mont-royal-golf-shoes-grey-crimson/
41.5 now represents /product/native-miller-men-us-10-gray-loafer-uk-9-eu-43/
42.5 now represents /product/adidas-marathon-10-tr-sneakers-for-men-new-us-size-10-5/?unapproved=9&moderation-hash=2674d10a8e08bc4e9480bc37d3cb2e71
43.5 now represents /?customize_changeset_uuid=9231fe8a-69af-4768-9d46-28c3d5bc5acd&customize_theme=cs00487_fyp&customize_messenger_channel=preview-2&customize_autosaved=on
44.5 now represents /product/maui-and-sons-david-men-open-toe-synthetic-flip-flop-sandal/
45.5 now represents /product/toms-mens-classic-burlap-slip-on-casual-loafer-shoe-black-us-9/
46.5 now represents /product/nike-womens-victory-compression-sports-bra/?unapproved=11&moderation-hash=07ee93696f5cc9abb19f626b0ff99b2c
47.5 now represents /product/new-mens-nike-lunar-cypress-golf-shoes-style-652522-any-size-any-color/
48.5 now represents /product/allegra-k-womens-open-toe-chunky-mid-heel-ankle-strap-sandals-burgundy-size-6/?unapproved=4&moderation-hash=9df1607f77d01efdc4ed56dcb77ae5fe
49.5 now represents /product/hunter-boys-original-kids-knee-high-rubber-rain-boot/
50.5 now represents /product/alfani-jeules-women-pointed-toe-synthetic-white-heels/?unapproved=3&moderation-hash=f61239d938595d370ddc9f2c3e0c1881
51.5 now represents /product/ugg-classic-short-quilted-black-patent-leather-boots-size-6-new-in-box/
52.5 now represents /product/michael-michael-kors-winter-mid-boot-women-us-9-black-pre-owned-1905/
53.5 now represents /product/adidas-marathon-10-tr-sneakers-for-men-new-us-size-10-5/
54.5 now represents /product/carlos-santana-hanna-2-women-shoes-black-fashion-knee-high-boots-sz-7-5-m/
55.5 now represents /?customize_changeset_uuid=237f6be3-5ab8-4c0e-b16a-27f144607a5c&customize_theme=cs00487_fyp&customize_messenger_channel=preview-4&customize_autosaved=on
56.5 now represents /product/woolrich-men-beebe-lace-up-boots/
57.5 now represents /product/vionic-with-orthaheel-technology-womens-karina/
58.5 now represents /product/bedroom-athletics-womens-keira-sheepskin-button-detail-thong-slippers/
59.5 now represents /product-category/all-womens-shoes/page/20/
60.5 now represents /product/basic-editions-womens-skinny-jeggings/
61.5 now represents /product/coggs-mens-size-10-tan-winter-zip-up-boots/
62.5 now represents /product/filament-metric-men-round-toe-suede-skate-shoe/
63.5 now represents /product/academie-cheer-cm-v-saddle-school-shoes-navy-medium-size-10-5/
64.5 now represents /?customize_changeset_uuid=14386d20-e893-4704-a509-dcaf2a0319b1&customize_theme=cs00487_fyp&customize_messenger_channel=preview-3&customize_autosaved=on
65.5 now represents /?customize_changeset_uuid=14386d20-e893-4704-a509-dcaf2a0319b1&customize_theme=cs00487_fyp&customize_messenger_channel=preview-5&customize_autosaved=on
66.5 now represents /?customize_changeset_uuid=9231fe8a-69af-4768-9d46-28c3d5bc5acd&customize_theme=cs00487_fyp&customize_messenger_channel=preview-0
67.5 now represents /checkout/
68.5 now represents /product/burberry-be4144-sunglasses/
69.5 now represents /product/robert-wayne-highway-men-round-toe-canvas-loafer/
70.5 now represents /cart/
71.5 now represents /product/caparros-parisian-women-pointed-toe-synthetic-gold-heels/
************************** Product Categories ***********************
1.5 now represents product_cat-shoes-accessories
2.5 now represents product_cat-shoes
3.5 now represents product_cat-womens-socks
4.5 now represents product_cat-all-mens-shoes
5.5 now represents product_cat-womens-shoes
6.5 now represents product_cat-shoes-jewelry
7.5 now represents product_cat-accessories
8.5 now represents product_cat-clothing
****************************** User ID ******************************
1 now represents ee11cbb19052e40b07aac0ca060c23ee
2 now represents 26cf679c236163dceda8921e9e3295aa
3 now represents 43b90920409618f188bfc6923f16b9fa
4 now represents 4ec651f0493b0ea958aa5238220ef2fa
5 now represents 55feb130be438e686ad6a80d12dd8f44
6 now represents 5ce4d191fd14ac85a1469fb8c29b7a7b
7 now represents 81162e1ef3d93f96b36d3712ca52bca5
8 now represents 9a7ff71f4343f04a6474f3566109a243
9 now represents 9e8486cdd435beda9a60806dd334d964
10 now represents b09315ea09c6d3b5680094257f1f70e4
11 now represents d20caec3b48a1eef164cb4ca81ba2587
12 now represents df9276a6b0ebd2819edf97b6706cffbb
13 now represents e64cfa3fd59e32df57003c7401f48c99
14 now represents 033f7f6121501ae98285ad77f216d5e7
15 now represents 3079e3991f94d1b3b21b894f329d369d
16 now represents 24e7cf4b58c17610001bf72a02db519e
17 now represents f04af61b3f332afa0ceec786a42cd365
18 now represents 2f72319caec5d639aead26fc77b5ef67
19 now represents e39a512929d854e965176f73235dfc83
20 now represents 9ce44f88a25272b6d9cbb430ebbcfcf1
21 now represents a2b14389d02e3cd6e4e115d40742e55f
22 now represents 0baea2f0ae20150db78f58cddac442a9
23 now represents 6636e1322531bd6a9bf66f3c3aabd8d6
24 now represents 503c6e4c3a55eed3c45ab5ccfbbb5cbd
25 now represents aea119b29e2487c947a6ca5a9390f2a7
26 now represents becfb907888c8d48f8328dba7edf6969
27 now represents d23949a122499d1209be9a00deec30b5
28 now represents 527d60cd4715db174ad56cda34ab2dce
29 now represents 231082b70a29088d35cd3041db84c863
30 now represents 30e6d8432ce54710f9c09f305e7b9829
31 now represents 96be325cf35649f0730df921686aae2c
32 now represents edf1439075a83a447fb8b630ddc9c8de
33 now represents 0ff8da9954b3e491404374302f7499ac
