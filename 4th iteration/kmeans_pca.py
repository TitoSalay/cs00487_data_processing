import csv
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn import decomposition
from sklearn import preprocessing
from matplotlib import pyplot as plt
from matplotlib_venn import venn3, venn3_circles
from sklearn.cluster import KMeans
from sklearn import neighbors

with open('results.txt', 'w') as results:
    with open('numerical_data.csv') as num_data:
        data_dict = csv.DictReader(num_data)
        table = pd.DataFrame(data_dict)
        col = table.columns
        rows = table.index

        print(table.head()) # shows the first 5 rows with all the columns of the table
        print(table.shape) # shows how many (rows, columns)

    print('-----------------------------------------')
    print('scaling data.................')
    scaler = preprocessing.StandardScaler()
    scaled_table = scaler.fit_transform(table)
    new_table = pd.DataFrame(table, columns=col)

    scaledData = preprocessing.scale(new_table.T)

    print('-----------------------------------------')
    print('Applying PCA...............')
    pca_obj = PCA(n_components=9)
    pca_obj.fit(scaledData)



    # Here we are getting the coordinates for a pca graph based on the scaled data and the 
    # loading scores. 
    pca_coor = pca_obj.transform(scaledData) 

    # percentage of variation that each principal component accounts for.
    # Percentage Variation Per Component (pvpc)

    pvpc = np.round(pca_obj.explained_variance_ratio_ * 100, decimals=1)

    # Here we are creating a label for each principal component that could be used 
    # in the graph that will be plotted.

    # note: lbl stands for labels, its a naming convention I was familiar with when first learning
    # Visual Basic. lbl with an s in this case is just to emphasize there are multiple labels.
    lbls = ['pc' + str(i) for i in range(1, len(pvpc)+1)]

    plt.bar(x=range(1,len(pvpc)+1), height=pvpc, tick_label=lbls)
    plt.ylabel('Percentage of Variance')
    plt.xlabel('The Principal Components')
    plt.title('Bar Plot')
    plt.show()


    pca_dataframe = pd.DataFrame(pca_coor, index=col, columns=lbls)
    # pc 1 and 2 are the most important ones

    print('writing PCA 1 values........')

    results.write('========================= PCA results: ============================\n')
    results.write(str(pca_dataframe['pc1']))

    plt.scatter(pca_dataframe.pc1, pca_dataframe.pc2)
    plt.title('PCA Graph')
    plt.xlabel('pc1 - {0}%'.format(pvpc[0]))
    plt.ylabel('pc2 - {0}%'.format(pvpc[1]))

    for dimension in pca_dataframe.index:
        plt.annotate(dimension, (pca_dataframe.pc1.loc[dimension], pca_dataframe.pc2.loc[dimension]))
    plt.show()

    # Now we can try see which one of the dimensions made the biggest difference in the
    # way the clusters in the data are distincive amongst each other.

    k_means = KMeans(n_clusters=3, random_state=0)
    predicted = k_means.fit_predict(table)
    centroids = k_means.cluster_centers_

    table['cluster'] = predicted


    print(table.head())
 
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        tableX = table[table.cluster == 0]
        tableY = table[table.cluster == 1]
        tableZ = table[table.cluster == 2]


        print('Writing Kmeans clustering results............')

        results.write('=============== Kmeans Clustering results =========================\n')
        results.write('---------------------- Cluster 1 ----------------------------------\n')
        results.write(str(tableX) + '\n')

        results.write('---------------------- Cluster 2 ----------------------------------\n')
        results.write(str(tableY) + '\n')

        results.write('---------------------- Cluster 3 -----------------------------------\n')
        results.write(str(tableZ) + '\n')


    plt.scatter(tableX['product-1'], tableX['product-2'], color="red")
    plt.scatter(tableY['product-1'], tableY['product-2'], color="blue")
    plt.scatter(tableZ['product-1'], tableZ['product-2'], color="purple")
   
    plt.xlabel('product-1')
    plt.ylabel('product-2')
    plt.show()

    print('Kmeans clustering done!')

    venn3(subsets=(table['cluster']), set_labels=('Cluster 0', 'Cluster 1', 'Cluster 2'))
    plt.title('User Profiles')
    plt.show()
    
# cluster 0 = 1,3,4,5,6,8,9,10,12,13,15,16,17,19,20,21,22,23,24,26,27,28,29,30,31,32,33
# cluster 1 = 2,3,6,7,13,14,25,26
# cluster 2 = 1,2,4,5,7,8,10,11,12,14,15,17,18,19,24,25
   


