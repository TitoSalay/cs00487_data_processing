import csv
processed = []
columns = ['user_id', 'client_id','Hits','Adding a product to the cart (Goal 2 Completions)',
'Adding a product to the cart (Goal 2 Conversion Rate)','Returning user (Goal 4 Completions)', 'Returning user (Goal 4 Conversion Rate)',
'User placing an order (Goal 3 Conversion Rate)', 'User placing an order (Goal 3 Completions)']
i = 0

# Completed the main dimentions of the GA data
# getting details per unique user

print('processing the users data......')
with open('users.csv') as users_data:
    user_dict = csv.DictReader(users_data)
    users = list(user_dict)
    for user in users:
        processed.append({
            'user_id': user['User ID'],
            'client_id':user['client ID'],
            'Hits': user['Hits'],
            'Adding a product to the cart (Goal 2 Completions)': user['Adding a product to the cart (Goal 2 Completions)'],
            'Adding a product to the cart (Goal 2 Conversion Rate)': user['Adding a product to the cart (Goal 2 Conversion Rate)'],
            'Returning user (Goal 4 Completions)': user['Returning user (Goal 4 Completions)'],
            'Returning user (Goal 4 Conversion Rate)': user['Returning user (Goal 4 Conversion Rate)'],
            'User placing an order (Goal 3 Conversion Rate)': user['User placing an order (Goal 3 Conversion Rate)'],
            'User placing an order (Goal 3 Completions)': user['User placing an order (Goal 3 Completions)']
        })
    
print('finished processing the users data')
print('----------------------------------------------------')
print('processing events data.....')
with open('events.csv') as events_data:
    events_dict = csv.DictReader(events_data)
    events = list(events_dict)

    for p in processed:
        for event in events:
            i += 1
            if p['user_id'] == event['User ID']:
                column = 'event-' + str(i)
                column2 = 'event-cat' + str(i)
                column3 = 'sessions'
                
                if column in columns:
                    p[column] = event['Event Action']
                elif column2 in columns:
                    p[column2] = event['Event Category']
                elif column3 in columns:
                    p[column3] = event['Sessions']
                else:
                    columns.append(column)
                    columns.append(column2)
                    columns.append(column3)
             
                    p[column] = event['Event Action']
                    p[column2] = event['Event Category']
                    p[column3] = event['Sessions']
            else:
                i = 0

print('finished processing the events data.')
print('----------------------------------------------------')
print('processing products data......')
with open('products.csv') as products_data:
    products_dict = csv.DictReader(products_data)
    products = list(products_dict)

    for p in processed:
        for product in products:
            i += 1
            if p['user_id'] == product['User ID']:
                column = 'product-' + str(i)
                column2 = 'hovered in category ' + str(i)
                column3 = 'hovered out category' + str(i)
                if column in columns:
                    p[column] = product['Product ID']
                elif column2 in columns:
                    p[column2] = product['Product Categories in']
                elif column3 in columns:
                    p[column3] = product['Product Categories Out']
                else:
                    columns.append(column)
                    columns.append(column2)
                    columns.append(column3)
                    p[column] = product['Product ID']
                    p[column2] = product['Product Categories in']
                    p[column3] = product['Product Categories Out']
            else:
                i = 0
print('finished processing products data.')
print('----------------------------------------------------')
print('processing pages data........')

print('im here')
with open('page.csv') as page_visited:
    pages_dict = csv.DictReader(page_visited)
    pages = list(pages_dict)


    for p in processed:
        for page in pages:
            i +=1
            
            if p['user_id'] == page['User ID']:
                 column = 'page-' + str(i)
                 column2 = 'content-' + str(i)
                 if column in columns:
                    p[column] = page['Page']
                 if column2 in columns:
                    p[column2] = page['Demographics (Content Group)']
                 else:
                    columns.append(column)
                    columns.append(column2)
                    p[column] = page['Page']
                    p[column2] = page['Demographics (Content Group)']
            else:
                i = 0
print('finished processing data for the pages')
print('----------------------------------------------------')
print('creating data ....')
try:
    with open('data.csv', 'w') as data:
        writer = csv.DictWriter(data, fieldnames=columns)
        writer.writeheader()
        for p in processed:
            writer.writerow(p)
except IOError:
    print("I/O error") 
print('finished!')