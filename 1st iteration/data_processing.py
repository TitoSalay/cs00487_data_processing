import csv
processed = []
columns = ['id', 'username','event-1','event-2','event-3',
'event-4', 'event-5','event-6','event-7','event-8','event-9','event-10','product-1',
'product-2','product-3','product-4','product-5','product-6','product-7','product-8',
'product-9','product-10','page-1','page-2','page-3','page-4','page-5','Sessions with Event',
'Total Events','Adding a product on to the cart (Goal 4 Conversion Rate)',
'Adding a product on to the cart (Goal 4 Completions)', 'Sessions', 'Pages / Session']
i = 0

# Completed the main dimentions of the GA data
# getting details per unique user
with open('users.csv') as users_data:
    user_dict = csv.DictReader(users_data)
    users = list(user_dict)
    
    for user in users:
        processed.append({
            'id': user['ID'],
            'username': user['WordPress_Username']#,
        })

with open('events.csv') as events_data:
    events_dict = csv.DictReader(events_data)
    events = list(events_dict)

    for p in processed:
        for event in events:
            i += 1
            if p['id'] == event['ID']:
                p['event-' + str(i)] = event['Event Action']
            else:
                i = 0
           
with open('products.csv') as products_data:
    products_dict = csv.DictReader(products_data)
    products = list(products_dict)

    for p in processed:
        for product in products:
            i += 1
            if p['id'] == product['ID']:
                p['product-' + str(i)] = product['Product ID']
            else:
                i = 0

with open('pages.csv') as page_visited:
    pages_dict = csv.DictReader(page_visited)
    pages = list(pages_dict)

    for p in processed:
        for page in pages:
            i +=1
            if p['id'] == page['ID']:
                 p['page-' + str(i)] = page['Page']
            else:
                i = 0

 
for p in processed:
    for user in users:
        p['Sessions with Event'] = user['Sessions with Event']
        p['Total Events'] = user['Total Events']
        p['Adding a product on to the cart (Goal 4 Conversion Rate)'] = user['Adding a product on to the cart (Goal 4 Conversion Rate)']
        p['Adding a product on to the cart (Goal 4 Completions)'] = user[ 'Adding a product on to the cart (Goal 4 Completions)']
    for event in events:
        if p['id'] == event['ID']:
            p['Sessions'] = event['Sessions']
    for page in pages:
        if p['id'] == page['ID']:
            p['Pages / Session'] = page['Pages / Session']
try:
    with open('data.csv', 'w') as data:
        writer = csv.DictWriter(data, fieldnames=columns)
        writer.writeheader()
        for p in processed:
            writer.writerow(p)
except IOError:
    print("I/O error") 