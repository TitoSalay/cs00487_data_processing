import csv
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn import decomposition
from sklearn import preprocessing
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans

#IMPORTANT ! dont forget to remove the %, the GA.4. and fill all the empty cells with 0
with open('numerical_data.csv') as num_data:
    data_dict = csv.DictReader(num_data)
    table = pd.DataFrame(data_dict)
    col = table.columns
    rows = table.index

    print(table.head()) # shows the first 5 rows with all the columns of the table
    print(table.shape) # shows how many (rows, columns)

scaler = preprocessing.StandardScaler()
scaled_table = scaler.fit_transform(table)
new_table = pd.DataFrame(scaled_table, columns=col)

scaledData = preprocessing.scale(new_table.T)
pca_obj = PCA()
# the goal is to have the as much information as I can, therefore two principal components 
# is the solution.
pca_obj.fit(scaledData)

# Here we are getting the coordinates for a pca graph based on the scaled data and the 
# loading scores. 
pca_coor = pca_obj.transform(scaledData) 

# percentage of variation that each principal component accounts for.
# Percentage Variation Per Component (pvpc)

pvpc = np.round(pca_obj.explained_variance_ratio_ * 100, decimals=1)

# Here we are creating a label for each principal component that could be used 
# in the graph that will be plotted.

# note: lbl stands for labels, its a naming convention I was familiar with when first learning
# Visual Basic. lbl with an s in this case is just to emphasize there are multiple labels.
lbls = ['pc' + str(i) for i in range(1, len(pvpc)+1)]

plt.bar(x=range(1,len(pvpc)+1), height=pvpc, tick_label=lbls)
plt.ylabel('Percentage of Variance')
plt.xlabel('The Principal Components')
plt.title('Bar Plot')
plt.show()


pca_dataframe = pd.DataFrame(pca_coor, index=col, columns=lbls)
# pc 1 and 2 are the most important ones

print(pca_dataframe['pc1'])

plt.scatter(pca_dataframe.pc1, pca_dataframe.pc2)
plt.title('PCA Graph')
plt.xlabel('pc1 - {0}%'.format(pvpc[0]))
plt.ylabel('pc2 - {0}%'.format(pvpc[1]))

for dimension in pca_dataframe.index:
    plt.annotate(dimension, (pca_dataframe.pc1.loc[dimension], pca_dataframe.pc2.loc[dimension]))
plt.show()

# Now we can try see which one of the dimensions made the biggest difference in the
# way the clusters in the data are distincive amongst each other.

k_means = KMeans(n_clusters=2, random_state=0)
predicted = k_means.fit_predict(pca_dataframe)
centroids = k_means.cluster_centers_

pca_dataframe['cluster'] = predicted

print(pca_dataframe.head())

tableX = pca_dataframe[pca_dataframe.cluster == 0]
tableY = pca_dataframe[pca_dataframe.cluster == 1]
# tableZ = pca_dataframe[pca_dataframe.cluster == 2]

plt.scatter(tableX['pc1'], tableX['pc2'], color="red")
plt.scatter(tableY['pc1'], tableY['pc2'], color="blue")
# plt.scatter(tableZ['pc1'], tableZ['pc2'], color="purple")
plt.xlabel('PC1')
plt.ylabel('PC2')
plt.show()
