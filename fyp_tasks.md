# Taks acheived #

# Google Tag Manager #
* added some custom tags  with some javascript 
    * added a tag to remove the default checkout valdiation and redirect to the order received
* added some GA event clicks , made sure they worked.
* explain the custom dimensions that you have used.

# WordPress #
* create a WordPress child theme
* Data processing, csv files of products from Datafinity containing more than 1000 products 
    * remove unecessary columns
    * remove duplicated data
    * remove as many products without images as possible

* Getting Username from the theme
    * Get the username from a WordPress cookie.

* Python
    * Processing the data to make all data fit in one row
    * Turn non numerical data into numerical data
    * implemented K-Means clustering.
    * used Microsoft excel to fill in empty values with zero.
    * used microsoft excel to remove the WordPress username.
    * userd microsoft excel to remove "E+ values"
    * microsoft excel to replace empty values
    * excluded the products removed because only one user consistentenly removed products.
    * for scaling purposes, even the client id values had to be changed.


# Reason why I removed a lot of the metrics and only kept hits #
THe goal is to recommend products based on each user interaction, which is the only metric that is relevant. Anything else would not help in terms of seeing what users do with each interaction on the e-commerce site.

-------------- Challenges ------------------------

* Data processing (wordpress of over 1000 products).
* Understanding Google analytics / Google tag manager.
* getting tags to work properly 
* getting data suitable for machine learning
* getting a unique id and username for each interaction.
* getting custom events to work ( hover , user sign in )

------ What needs to be done ------------

1. review writting style
2. Talk about the User ID
3. Add PCA to Literature review.
4. Talk about the client ID.
5. Break your literature review into multiple parts, (include the literature review in the iterations.)
6. change any referrence to the user id to represent the client ID.
    * justify not putting the client id from the start by saying that you was seeing if it will work by having the client id and the username seperate.

------------- New Tasks ---------------

1. Added the product tags
2. changed the custom tags on google tag manager to work with tags
3. added the total price spent for each customer.
4. Added new content groups using the extraction method.

change the percentages within the spreadsheet programmaticaly or manually (need to do that).