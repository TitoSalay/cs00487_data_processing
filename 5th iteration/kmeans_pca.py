import csv
import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn import decomposition
from sklearn import preprocessing as pre
from sklearn.neural_network import MLPClassifier as mlp
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans
from sklearn import neighbors

with open('./reports/results.txt', 'w') as results:
    dataset = pd.read_csv('./output/data.csv')
    dimensions = dataset.columns
    encoder = pre.LabelEncoder()

    numerical_data = dataset.apply(encoder.fit_transform)
    
    print(numerical_data.head()) # shows the first 5 rows with all the columns of the table
    print(numerical_data.shape) # shows how many (rows, columns)

    print('-----------------------------------------')
    print('scaling data.................')
    scaler = pre.StandardScaler()
    scaled_table = scaler.fit_transform(numerical_data)
    # new_table = pd.DataFrame(table, columns=col)

    scaledData = pre.scale(numerical_data.T)

    print('-----------------------------------------')
    print('Applying PCA...............')
    pca_obj = PCA()
    pca_obj.fit(scaledData)


    # Here we are getting the coordinates for a pca graph based on the scaled data and the 
    # loading scores. 
    pca_coor = pca_obj.transform(scaledData) 

    # percentage of variation that each principal component accounts for.
    # Percentage Variation Per Component (pvpc)

    pvpc = np.round(pca_obj.explained_variance_ratio_ * 100, decimals=1)

    # Here we are creating a label for each principal component that could be used 
    # in the graph that will be plotted.

    # note: lbl stands for labels, its a naming convention I was familiar with when first learning
    # Visual Basic. lbl with an s in this case is just to emphasize there are multiple labels.
    lbls = ['pc' + str(i) for i in range(1, len(pvpc)+1)]

    plt.bar(x=range(1,len(pvpc)+1), height=pvpc, tick_label=lbls)
    plt.ylabel('Percentage of Variance')
    plt.xlabel('The Principal Components')
    plt.title('Bar Plot')
    plt.show()


    pca_dataframe = pd.DataFrame(pca_coor, index=dimensions, columns=lbls)
    # pc 1 and 2 are the most important ones

    print('writing PCA 1 values........')

    results.write('========================= PCA results: ============================\n')
    results.write(str(pca_dataframe['pc1']))

    plt.scatter(pca_dataframe.pc1, pca_dataframe.pc2)
    plt.title('PCA Graph')
    plt.xlabel('pc1 - {0}%'.format(pvpc[0]))
    plt.ylabel('pc2 - {0}%'.format(pvpc[1]))

    for dimension in pca_dataframe.index:
        plt.annotate(dimension, (pca_dataframe.pc1.loc[dimension], pca_dataframe.pc2.loc[dimension]))
    plt.show()

    # Now we can try see which one of the dimensions made the biggest difference in the
    # way the clusters in the data are distincive amongst each other.

    k_means = KMeans(n_clusters=5, random_state=0, algorithm='full')
    predicted = k_means.fit_predict(numerical_data)
    centroids = k_means.cluster_centers_

    # Now initializing the multi layer percepetron object for a neural network
    mlp = mlp(hidden_layer_sizes=(30,30,30))
    mlp.fit(numerical_data, predicted)


    numerical_data['cluster'] = predicted
    dataset['cluster'] = predicted

 
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        tableX = numerical_data[numerical_data.cluster == 0]
        tableY = numerical_data[numerical_data.cluster == 1]
        tableZ = numerical_data[numerical_data.cluster == 2]
        tableW = numerical_data[numerical_data.cluster == 3]
        tableS = numerical_data[numerical_data.cluster == 4]

    plt.scatter(tableX['user_id'], tableX['client_id'], color="red")
    plt.scatter(tableY['user_id'], tableY['client_id'], color="blue")
    plt.scatter(tableZ['user_id'], tableZ['client_id'], color="purple")
    plt.scatter(tableW['user_id'], tableW['client_id'], color="pink")
    plt.scatter(tableS['user_id'], tableS['client_id'], color="green")
   
    plt.xlabel('User ID')
    plt.ylabel('Client ID')
    plt.title('User Profiles')
    plt.show()

    print('writing user profile data....')

    profile1 = dataset[dataset.cluster == 0]
    profile1.to_csv('./user_profiles/profile1.csv',index=False)

    profile2 = dataset[dataset.cluster == 1]
    profile2.to_csv('./user_profiles/profile2.csv',index=False)

    profile3 = dataset[dataset.cluster == 2]
    profile3.to_csv('./user_profiles/profile3.csv',index=False)

    profile4 = dataset[dataset.cluster == 3]
    profile4.to_csv('./user_profiles/profile4.csv',index=False)

    profile5 = dataset[dataset.cluster == 4]
    profile5.to_csv('./user_profiles/profile5.csv',index=False)

    print('finished writing user profile data')
    print('now writing clusters data for classification....')


